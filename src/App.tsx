import React from 'react';
import './App.scss';
import { employees } from './data/employees';

import Employees from './components/Employees';

function App() {
  return (
    <div className="App">
      <Employees data={employees} />
    </div>
  );
}

export default App;
