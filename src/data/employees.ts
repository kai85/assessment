export enum TransportType {
  Bike = "Bike",
  Bus = "Bus",
  Car = "Car",
  Train = "Train"
}

export const employees = [
   {
    "id": 1,
    "name": "Paul",
    "transport": TransportType.Car,
    "distance": 60,
    "workdays": 5
   },
   {
    "id": 2,
    "name": "Martin",
    "transport": TransportType.Bus,
    "distance": 8,
    "workdays": 4
   },
   {
    "id": 3,
    "name": "Jeroen",
    "transport": TransportType.Bike,
    "distance": 9,
    "workdays": 5
   },
   {
    "id": 4,
    "name": "Tineke",
    "transport": TransportType.Bike,
    "distance": 4,
    "workdays": 3
   },
   {
    "id": 5,
    "name": "Arnout",
    "transport": TransportType.Train,
    "distance": 23,
    "workdays": 5
   },
   {
    "id": 6,
    "name": "Matthijs",
    "transport": TransportType.Bike,
    "distance": 11,
    "workdays": 4.5
   },
   {
    "id": 7,
    "name": "Rens",
    "transport": TransportType.Car,
    "distance": 12,
    "workdays": 5
   }
 ];
 
