import { TransportType } from '../data/employees';

const compensationRates = [
  {
    transport: TransportType.Bike,
    rate: 0.5,
  },
  {
    transport: TransportType.Bus,
    rate: 0.25,
  },
  {
    transport: TransportType.Train,
    rate: 0.25,
  },
  {
    transport: TransportType.Car,
    rate: 0.1,
  },
];

function getTransportType(transportType: TransportType) {
  return (
    compensationRates.find((x) => x.transport === transportType) ||
    compensationRates[0]
  );
}

function defaultRate(distance: number, rate: number) {
  return distance * 2 * rate;
}

function bikeRate(distance: number, rate: number) {
  const BIKE_DOUBLE_RATE_BEGIN = 5;
  const BIKE_DOUBLE_RATE_RANGE = 5;

  if (distance < BIKE_DOUBLE_RATE_BEGIN) {
    return defaultRate(distance, rate);
  }

  const doubleRateDistance = Math.min(
    distance - BIKE_DOUBLE_RATE_BEGIN,
    BIKE_DOUBLE_RATE_RANGE
  );
  const doubleRate = doubleRateDistance * rate * 2;
  const singleRate = (distance - doubleRateDistance) * rate;

  return (doubleRate + singleRate) * 2;
}

export function compensation(
  transport: TransportType,
  distance: number,
  workdays: number
) {
  const rate = getTransportType(transport).rate;

  if (transport === TransportType.Bike) {
    return bikeRate(distance, rate) * workdays;
  }

  const dailyRate = defaultRate(distance, rate);

  return (dailyRate * workdays).toFixed(2);
}
