import React, { ReactNode } from 'react';
import cx from 'classnames';
import { compensation } from '../functions/compensation';

interface EmployeesProps {
  children?: ReactNode;
  className?: string;
  data?: unknown[];
}

const Employees = (props: EmployeesProps) => {
  const { className = null, data } = props;

  const employeesClass = cx('employees', {
    [`${className}`]: className,
  });

  return (
    <div className={employeesClass}>
      <table>
        <thead>
          <tr>
            <th>Employee</th>
            <th>Transport</th>
            <th>Distance (km/one way)</th>
            <th>Workdays per week</th>
            <th colSpan={2}>Compensation per week / day</th>
          </tr>
        </thead>

        <tbody>
          {!data && (
            <tr>
              <td colSpan={6}>
                <div className="text-center">No data found :(</div>
              </td>
            </tr>
          )}

          {data?.map((employee: any, index: number) => (
            <tr key={index}>
              <td>{employee.name}</td>
              <td>{employee.transport}</td>
              <td>{employee.distance}</td>
              <td>{employee.workdays}</td>
              <td className="text-right">
                &euro;
                {compensation(
                  employee.transport,
                  employee.distance,
                  employee.workdays
                )}
              </td>
              <td className="text-right">
                &euro;
                {compensation(employee.transport, employee.distance, 1)}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Employees;
